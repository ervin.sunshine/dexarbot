package order

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"time"
	"strings"

	types "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/types/msg"
	"github.com/binance-chain/go-sdk/types/tx"
	"github.com/binance-chain/go-sdk/client/transaction"
	"github.com/binance-chain/go-sdk/client/websocket"
	"gitlab.com/zlyzol/dexarbot/ctx"
)

// OrderSide - BUY or SELL
var Side = struct {
	BUY  int8
	SELL int8
}{1, 2}
type PriceVol struct {
	Price  float64
	Amount float64
}
func (pv *PriceVol) String() string {
	if pv == nil {
		return "nil"
	}
	return fmt.Sprintf("%v @ %v = %v", pv.Amount, pv.Price, pv.Mul())
}
func (pv *PriceVol) Str(asset1, asset2 string) string {
	if pv == nil {
		return "nil"
	}
	return fmt.Sprintf("%v %s @ %v = %v %s", pv.Amount, asset1, pv.Price, pv.Mul(), asset2)
}
func (pv *PriceVol) StrS(os int8, asset1, asset2 string) string {
	if pv == nil {
		return "nil"
	}
	return fmt.Sprintf("%v %v %s @ %v = %v %s", func(os int8) string {
		if os == Side.BUY {
			return "BUY"
		} else {
			return "SELL"
		}
	}(os),
	pv.Amount, asset1, pv.Price, pv.Mul(), asset2)
}
func (pv *PriceVol) Mul() float64 {
	if pv == nil {
		return 0
	}
	return ctx.F64xF8F64((pv.Price * pv.Amount))
}
func (pv *PriceVol) PriceI64() int64 {
	return ctx.F64xI64(pv.Price)
}
func (pv *PriceVol) AmountI64() int64 {
	return ctx.F64xI64(pv.Amount)
}
// orderState - Other, Ack, Partial, Full, IOCExpire
var orderState = struct {
	OTHER int8
	ACK  int8
	PART int8
	FULL int8
	IOCE int8
	IOCN int8
}{0, 1, 2, 3, 4, 5}
type OrderStatus struct {
	Os int8
	Pv PriceVol
}
func (os OrderStatus) String() string {
	return fmt.Sprintf("order status: %v | %v", os.GetResStr(), os.Pv)
}
func (os *OrderStatus) GetResStr() string {
	switch os.Os {
		case orderState.ACK: return "ACK"
		case orderState.PART: return "PART"
		case orderState.FULL: return "FULL"
		case orderState.IOCE: return "IocExpired"
		case orderState.IOCN: return "IocNofill"
		default: return "OTHER"
	}
}
// Order execution struct
type Order struct {
	ctx       *ctx.Context
	tAsset    string
	qAsset    string
	asset	  string
	orderSide int8
	in		  PriceVol
	res 	  OrderStatus
	orderId   string
	acc		  *ctx.Account
}
type OrderExecutor interface {
	Execute(tAsset, qAsset string, amount, limit float64, orderSide int8) (*Order, error)
	BurstExecute(orders []*Order) error
}
type Executor struct {
	ctx *ctx.Context
	acc *ctx.Account
}
func NewExecutor(ctx *ctx.Context, acc *ctx.Account) Executor {
	return Executor{
		ctx: ctx,
		acc: acc,
	}
}
// NewOrder - creates new order struct
func NewOrder(ctx *ctx.Context, acc *ctx.Account, tAsset, qAsset string, amount, limit float64, orderSide int8) *Order {
	limit, amount = ctx.AdjustTickAndLot(tAsset+"_"+qAsset, limit, amount)
	o := Order{
		ctx:       ctx,
		tAsset:    tAsset,
		qAsset:    qAsset,
		asset:	   fmt.Sprintf("%s_%s", tAsset, qAsset),
		orderSide: orderSide,
		in:		   PriceVol{Amount: amount, Price: limit},
		acc:	   acc,
	}
	return &o
}
func (exe Executor) Execute(tAsset, qAsset string, amount, limit float64, orderSide int8) (*Order, error) {
	o := NewOrder(exe.ctx, exe.acc, tAsset, qAsset, amount, limit, orderSide)
	err := o.Execute()
	return o, err
}
func (exe Executor) BurstExecute(orders []*Order) error {
	for _, o := range orders {
		err := o.BurstExecute()
		if err != nil {
			return err
		}
		//time.Sleep(400*time.Millisecond) // to not get error "API rate limit exceeded"
	}
	for _, o := range orders {
		err := o.getOrderResult()
		if err != nil {
			log.Printf("ERROR: Dex order execution - getOrderResult failed. %v. Error: %v", o.in.StrS(o.orderSide, o.tAsset, o.qAsset), err)
		}
	}
	return nil
}
// Execute - executes the order on Binance DEX
func (o *Order) Execute() error {
	log.Printf("Dex order execution: %v", o.in.StrS(o.orderSide, o.tAsset, o.qAsset))
	err := o.getOrderResult()
	if err != nil {
		log.Printf("ERROR: Dex order execution error: %v", err)
		return err
	}
	log.Printf("Dex order execution result: %v", o.res.Pv.StrS(o.orderSide, o.tAsset, o.qAsset))
	return nil
}
func (o *Order) BurstExecute() error {
	log.Printf("Binance CreateOrder call: %s (accNo: %v, accSeq: %v)", o.in.StrS(o.orderSide, o.tAsset, o.qAsset), o.acc.Number, o.acc.Sequence)
	res, err := o.CreateOrder(o.asset, o.orderSide, o.in.PriceI64(), o.in.AmountI64(), true, transaction.WithAcNumAndSequence(o.acc.Number, o.acc.Sequence))
	log.Printf("Binance CreateOrder call result: %+v, err: %v)", res, err)
	if err != nil && strings.Contains(err.Error(), "Invalid sequence") {
		log.Print("CreateOrder - updating sequence")
		o.acc.ReadBalances()
		log.Printf("CreateOrder - updating sequence to: %v", o.acc.Sequence)
		res, err := o.CreateOrder(o.asset, o.orderSide, o.in.PriceI64(), o.in.AmountI64(), true, transaction.WithAcNumAndSequence(o.acc.Number, o.acc.Sequence))
		log.Printf("Binance CreateOrder call result (after invalid sequence error): %+v, err: %v)", res, err)
	}
	if err != nil {
		for i := 0; err != nil && i < 20 && 
		(
			strings.Contains(err.Error(), "API rate limit exceeded") ||
			strings.Contains(err.Error(), "do not have enough token to lock"));
		i++ {
			log.Printf("API rate limit exceeded (%v-th)", i+1)
			time.Sleep(200 * time.Millisecond)
			res, err = o.CreateOrder(o.asset, o.orderSide, o.in.PriceI64(), o.in.AmountI64(), true, transaction.WithAcNumAndSequence(o.acc.Number, o.acc.Sequence))
			log.Printf("Binance CreateOrder call result (after api rate limit error): %+v, err: %v)", res, err)
		}
		if err != nil {
			log.Printf("Error: BurstExecute / CreateOrder call failed: %v", err)
			return err
		}
	}
	o.acc.Sequence++
	o.orderId = res.OrderId
	return nil
}
func (o *Order) getOrderResult() error {
	event := make(chan []*websocket.OrderEvent)
	oeQuit := make(chan struct{})
	immediately := make(chan struct{}, 1)
	defer close(oeQuit)
	defer close(immediately)
	var err error
	for i := 0; i < 3; i++ {
		err = o.ctx.Dex.SubscribeOrderEvent(o.acc.Wallet, oeQuit, func(ev []*websocket.OrderEvent) {
			//log.Print("SubscribeOrderEvent: event fired")
			event <- ev
		}, func (err error) {
			log.Printf("SubscribeOrderEvent: event error: %v", err)
		}, func() {
			log.Print("SubscribeOrderEvent: closed")
		})
		if err == nil {
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	if err != nil {
		log.Printf("SubscribeOrderEvent for wallet %v (asset %v_%v). No events for this order. Error: %v", o.acc.Wallet, o.tAsset, o.qAsset, err)
	}
	if o.orderId == "" {
		//res, err := o.ctx.Dex.CreateOrder(o.tAsset, o.qAsset, o.orderSide, ctx.F64xI64(o.limit), ctx.F64xI64(o.amount), true)
		res, err := o.CreateOrder(o.asset, o.orderSide, o.in.PriceI64(), o.in.AmountI64(), true)
		if err != nil {
			log.Printf("Error: CreateOrder call failed: %v", err)
			return err
		}
		o.orderId = res.OrderId
	} else {
		immediately <- struct{}{}
	}
	ticker := time.NewTicker(1000 * time.Millisecond)
	timeout := time.After(2000 * time.Millisecond)
	for {
		select {
		case <-o.ctx.Quit:
			return nil
		case evs := <-event:
			log.Printf("SubscribeOrderEvent: event (len: %v, ev: %v)", len(evs), evs)
			o.getOrderStatus(len(evs), func(i int) *OrderStatus {
				log.Printf("SubscribeOrderEvent: getos i = %v", i)
				if evs[i].OrderID != o.orderId {
					log.Printf("SubscribeOrderEvent: end 1 (%v != %v)", evs[i].OrderID, o.orderId)
					return nil
				}
				res := OrderStatus{}
				res.Os = o.getStatusFromString(evs[i].CurrentOrderStatus)
				if res.Os == orderState.IOCE {res.Os = orderState.FULL} // rewrite as final status FULL
				res.Pv = PriceVol{
					Price: ctx.F8xF64(evs[i].LastExecutedPrice), 
					Amount: ctx.F8xF64(evs[i].LastExecutedQty),
				}
				log.Printf("SubscribeOrderEvent: end 2")
				return &res
			})
			log.Printf("SubscribeOrderEvent %v", o.res)
			if o.res.Os == orderState.FULL || o.res.Os == orderState.OTHER {
				log.Printf("SubscribeOrderEvent: end 3")
				return nil
			}
		case <-immediately:
			log.Printf("Immediately getting status getOrderStatus")
			o.getOrderStatus(1, o.getDexOrderStatus)
			log.Printf("Immediately getting status %v", o.res)
			if o.res.Os == orderState.FULL || o.res.Os == orderState.OTHER {
				return nil
			}
		case <-ticker.C:
			log.Printf("Ticker event received -> getOrderStatus")
			o.getOrderStatus(1, o.getDexOrderStatus)
			log.Printf("Ticker event %v", o.res)
			if o.res.Os == orderState.FULL || o.res.Os == orderState.OTHER {
				return nil
			}
		case <-timeout:
			log.Printf("Timeout event received -> getOrderStatus")
			o.getOrderStatus(1, o.getDexOrderStatus)
			log.Printf("Timeout event %v", o.res)
			if o.res.Os == orderState.FULL {
				log.Printf("Warning: GetOrderResult TIMEOUT: Order executed. %v", o.res.Pv.Str(o.tAsset, o.qAsset))
				return nil
			}
			log.Printf("ERROR: GetOrderResult TIMEOUT: Order NOT executed. Asset: %v", o.tAsset)
			return fmt.Errorf("ERROR: GetOrderResult TIMEOUT: Order NOT executed. Asset: %v", o.tAsset)
		}
	}
}
func (o *Order) getOrderEventStatus(evs []*websocket.OrderEvent) OrderStatus {
	log.Print("Debug info getOrderEventStatus")
	// use LastExecutedQty,  LastExecutedPrice of OrderEvent
	var os OrderStatus
	var total float64
	var status string
	for _, ev := range evs {
		if ev.OrderID == o.orderId {
			log.Printf("Debug info OrderEvent ev1: %+v", ev)
			status = ev.CurrentOrderStatus
			am := ctx.F8xF64(ev.LastExecutedQty)
			if am > 0 {
				os.Pv.Amount = os.Pv.Amount + am
				total = total + am * ctx.F8xF64(ev.LastExecutedPrice)
			}
		}
	}
	if total > 0 {
		os.Pv.Price = total / os.Pv.Amount
	}
	os.Os = o.getStatusFromString(status)
	log.Printf("Debug info: getOrderEventStatus result %s", os)
	return os
}
func (o *Order) getStatusFromString(s string) int8 {
	if s == "Ack" {
		return orderState.ACK
	} else if s == "PartialFill" {
		return orderState.OTHER // orderState.PART
	} else if s == "IocExpire" {
		return orderState.IOCE
	} else if s == "FullyFill" {
		return orderState.FULL
	} else if s == "IocNoFill" {
		return orderState.IOCN
	}
	return orderState.OTHER
}
func (o *Order) getDexOrderStatus(int) *OrderStatus {
	var os OrderStatus
	ordr, err := o.ctx.Dex.GetOrder(o.orderId)
	log.Printf("Debug info GetOrder result: %+v", ordr)
	if err != nil {
		log.Printf("ERROR: GetOrder call failed: %v", err)
		os.Os = orderState.ACK // there was an error, but just wait and try again
		return &os
	}
	os.Pv.Amount = ctx.F8xF64(ctx.SxF8(ordr.CumulateQuantity))
	os.Pv.Price = ctx.F8xF64(ctx.SxF8(ordr.Price))
	os.Os = o.getStatusFromString(ordr.Status)
	log.Printf("Debug info: getDexOrderStatus result (%s),  %s", o.asset, os)
	return &os
}
func (o *Order) getOrderStatus(n int, getos func(i int) *OrderStatus) {
	var total float64
	for i := 0; i < n; i++ {
		res1 := getos(i)
		if res1 != nil && res1.Pv.Amount > 0 {
			o.res.Pv.Amount = o.res.Pv.Amount + res1.Pv.Amount
			total = total + res1.Pv.Mul()
		}
		o.res.Os = res1.Os
	}
	if total > 0 {
		o.res.Pv.Price = total / o.res.Pv.Amount
	}
	if o.res.Os == orderState.ACK {
		log.Printf("Debug info: getOrderStatus result 0 - ACK - wait os:(%s)", o.res)
		return // we do nothing, we wait - ACK
	}
	if o.res.Os != orderState.FULL && o.res.Os != orderState.IOCE { // unknown order state - try to cancel it and return fail
		if o.res.Os != orderState.IOCN {
			log.Printf("Debug info: getOrderStatus result OTHER - try to cancel order os:(%s)", o.res)
			_, err := o.ctx.Dex.CancelOrder(o.tAsset, o.qAsset, o.orderId, true)
			if err != nil {
				log.Printf("Debug info: CancelOrder failed on (OTHER status) order %v: %s", o.orderId, err)
			} else {
				log.Print("Debug info: getOrderStatus result -1 - OTHER - cancel order ok")
			}
		}
		log.Printf("Cannot successfully execute buy order on DEX - OrderId: %v, os:(%v)", o.orderId, o.res)
		o.res.Os = orderState.OTHER
		return // failed - OTHER
	} 
	log.Printf("Debug info: getOrderStatus result - success os:(%v)", o.res)
	if o.res.Os == orderState.FULL || o.res.Pv.Amount == o.in.Amount { // the order was fully filled and we have all trade info
		log.Printf("Debug info: getOrderStatus result 1 - Filled (Fully or Partialy / IOCE) (expected am: %v, got: %v), os:(%v)", o.in.Amount, o.res.Pv.Amount, o.res.GetResStr())
		o.res.Os = orderState.FULL // if it was IOCE we handle it as FULL
		return // success - FULL
	}
	// we have to get the order details for all trades & fill prices
	trades, err := o.GetTrades()
	if err != nil {
		log.Printf("ERROR: GetTrades failed: %v", err)
		o.res.Os = orderState.OTHER
		return // failed - OTHER
	}
	log.Printf("Debug info: getOrderStatus result - IOCE with not full trade info - calling recursively getOrderStatus. os:(%v)", o.res)
	// this is a bit tricky, calling recursively this func - risk of inifinite recursive loop, but it should not happen
	o.getOrderStatus(len(trades), func(i int) *OrderStatus {
		return &OrderStatus{
				Os: orderState.FULL, // this ensures that the recursive call returns
				Pv:PriceVol{
						Price: ctx.S8xF64(trades[i].Price), 
						Amount: ctx.S8xF64(trades[i].Quantity),
				}}
	})
}
func (o *Order) GetRes() *OrderStatus {
	return &o.res
}
// GetTrades returns transaction details
func (o *Order) GetTrades() ([]types.Trade, error) {
	bs := "buyerOrderId"
	if o.orderSide == Side.SELL { bs = "sellerOrderId" }
	qp := map[string]string{bs: o.orderId}
	log.Printf("Debug info: GetTrades: %+v", qp)
	resp, _, err := o.ctx.Dex.Get("/trades", qp)
	if err != nil {
		return nil, err
	}
	var trades types.Trades
	if err := json.Unmarshal(resp, &trades); err != nil {
		return nil, err
	}
	return trades.Trade, nil
}
func (o *Order) CreateOrder(asset string, op int8, price, quantity int64, sync bool, options ...tx.Option) (*transaction.CreateOrderResult, error) {
	newOrderMsg := NewCreateOrderMsg(
		o.ctx.GetAcc().Bech32,
		"",
		op,
		asset,
		price,
		quantity,
	)
	commit, err := o.broadcastMsg(newOrderMsg, sync, options...)
	if err != nil {
		return nil, err
	}
	type commitData struct {
		OrderId string `json:"order_id"`
	}
	var cdata commitData
	if sync {
		err = json.Unmarshal([]byte(commit.Data), &cdata)
		if err != nil {
			return nil, err
		}
	}

	return &transaction.CreateOrderResult{*commit, cdata.OrderId}, nil
}
// NewCreateOrderMsg constructs a new CreateOrderMsg
func NewCreateOrderMsg(sender types.AccAddress, id string, side int8, symbol string, price int64, qty int64) msg.CreateOrderMsg {
	return msg.CreateOrderMsg{
		Sender:      sender,
		ID:          id,
		Symbol:      symbol,
		OrderType:   msg.OrderType.LIMIT, // default
		Side:        side,
		Price:       price,
		Quantity:    qty,
//		TimeInForce: TimeInForce.GTC, // default
		TimeInForce: msg.TimeInForce.IOC, // IOC
	}
}
func (o *Order) broadcastMsg(m msg.Msg, sync bool, options ...tx.Option) (*tx.TxCommitResult, error) {
	n, err := o.ctx.Dex.GetNodeInfo()
	if err != nil {
		return nil, err
	}
	// prepare message to sign
	signMsg := &tx.StdSignMsg{
		ChainID:       n.NodeInfo.Network,
		AccountNumber: -1,
		Sequence:      -1,
		Memo:          "",
		Msgs:          []msg.Msg{m},
		Source:        tx.Source,
	}

	for _, op := range options {
		signMsg = op(signMsg)
	}

	if signMsg.Sequence == -1 || signMsg.AccountNumber == -1 {
		acc, err := o.ctx.Dex.GetAccount(o.acc.Wallet)
		if err != nil {
			return nil, err
		}
		signMsg.Sequence = acc.Sequence
		signMsg.AccountNumber = acc.Number
	}

	// special logic for createOrder, to save account query
	if orderMsg, ok := m.(msg.CreateOrderMsg); ok {
		orderMsg.ID = msg.GenerateOrderID(signMsg.Sequence+1, o.ctx.GetAcc().Bech32)
		signMsg.Msgs[0] = orderMsg
	}

	for _, m := range signMsg.Msgs {
		if err := m.ValidateBasic(); err != nil {
			return nil, err
		}
	}

	rawBz, err := o.ctx.Key.Sign(*signMsg)
	if err != nil {
		return nil, err
	}
	// Hex encoded signed transaction, ready to be posted to BncChain API
	hexTx := []byte(hex.EncodeToString(rawBz))
	param := map[string]string{}
	if sync {
		param["sync"] = "true"
	}
	commits, err := o.ctx.Dex.PostTx(hexTx, param)
	if err != nil {
		return nil, err
	}
	if len(commits) < 1 {
		return nil, fmt.Errorf("Len of tx Commit result is less than 1 ")
	}
	return &commits[0], nil
}

