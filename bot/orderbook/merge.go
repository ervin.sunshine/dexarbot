package orderbook

import (
	"log"
)

type ArbitrageOppCheckResult struct {
	BidNdx	int // last orderbook entry in the first (left) orderbook to use for the arbitrage trade, if positive, use asks, if negative, use bids
	AskNdx	int // last orderbook entry in the second (right) orderbook to use for the arbitrage trade, if positive, use asks, if negative, use bids
	BidLeft	float64	// amount to leave not used in the last used orderbook bid entry of the first (left) orderbook
	AskLeft	float64	// amount to leave not used in the last used orderbook ask entry of the second (right) orderbook
}

// Merge - merges two orderbook bid/ask entries
func Merge(ob1, ob2 *Orderbook, tickLot [2]uint64) *Orderbook {
	if ob1 == nil || ob2 == nil {
		return nil
	}
	ob := Orderbook{}
	ob.Bids = mergeEntries(ob1.Bids, ob2.Bids, tickLot)
	ob.Asks = mergeEntries(ob1.Asks, ob2.Asks, tickLot)
	return &ob
}

// mergeEntries - merges orderbook entries of two orderbook parts (bids / bids or asks / asks) and returns newly created merged orderbook part (bids or asks)
func mergeEntries(ob1, ob2 []OrderbookEntry, tickLot [2]uint64) []OrderbookEntry {
	if len(ob1) == 0 || len(ob2) == 0 {
		return nil
	}
	obex := make([]OrderbookEntry, 0, func (x, y int) int {
		if x < y {
			return y
		}
		return x
	} (len(ob1), len(ob2)) + 100)
	i := 0
	j := 0
	e1 := ob1[i]
	e2 := ob2[j]
	for {
		var ex OrderbookEntry
		ex.Price = e1.Price * e2.Price // price in base asset (BUSD)
		ex.Ex.Price = e2.Price // saved for using later in arbitrage trade as limit price
		tot1 := e1.Price * e1.Amount // amount in cross asset (BNB) of second orderbook entry
		if tot1 == e2.Amount { // (cross asset - BNB) if amounts of orderbook entries in both orderbooks are equal (rare case)
			ex.Amount = tot1 / e1.Price // result amount of traded asset (RUNE) for this full orderbook entry (BNB amount / RUNE price in BNB)
			ex.Ex.Amount = e2.Amount
			obex = append(obex, ex) // append resulting orderbook orderbook entry
			i++
			if i >= len(ob1) { // no more orderbook entry in first orderbook
				break
			}
			e1 = ob1[i] // use next orderbook entry of first orderbook
			j++
			if j >= len(ob2) { // no more orderbook entry in second orderbook
				break
			}
			e2 = ob2[j] // use next orderbook entry of second orderbook
		} else if tot1 > e2.Amount { // (cross asset - BNB) amount in first orderbook entry is greater then in the second
			ex.Amount = e2.Amount / e1.Price // result amount of traded asset (RUNE) for this partial orderbook entry (BNB amount / RUNE price in BNB)
			ex.Ex.Amount = e2.Amount
			obex = append(obex, ex) // append resulting orderbook entry
			e1.Amount = e1.Amount - ex.Amount // remaining (not used yet) RUNE in the first orderbook entry
			j++
			if j >= len(ob2) { // no more bids/asks in second orderbook
				break
			}
			e2 = ob2[j] // use next orderbook entry of second orderbook
		} else { // tot1 < e2.Amount 
			ex.Amount = tot1 / e1.Price // result amount of traded asset (RUNE) for this partial orderbook entry (BNB amount * RUNE price in BNB)
			ex.Ex.Amount = tot1
			obex = append(obex, ex) // append resulting orderbook entry
			e2.Amount = e2.Amount - tot1 // remaining (not used yet) RUNE in the second orderbook entry
			i++
			if i >= len(ob1) {
				break
			}
			e1 = ob1[i]
		}
	}
	return obex
}

// ChekArbOpportunity - checks if there is an arbitrage opportunity and returns max indexes of entries in orderbooks that can be used and the amount not to be used of the last enty
func ChekArbOpportunity(ob1, ob2 *Orderbook, max float64) (*ArbitrageOppCheckResult) {
	if len(ob1.Bids) == 0 || len(ob1.Asks) == 0 || len(ob2.Bids) == 0 || len(ob2.Asks) == 0 {
		return nil
	}
	maxLeft := max
	i, j := 0, 0
	bidLeft, askLeft := 0.0, 0.0
	var bids []OrderbookEntry
	var asks []OrderbookEntry
	if ob1.Bids[0].Price > ob2.Asks[0].Price { // we buy from ob2 & sell to ob1
		bids = ob1.Bids
		asks = ob2.Asks
	} else if ob2.Bids[0].Price > ob1.Asks[0].Price { // we buy from ob1 & sell to ob2
		bids = ob2.Bids
		asks = ob1.Asks
	} else { // here is no arbitrage opportunity
		log.Printf("No arbitrage opportunity at the moment ask/bid : ask/bid = %v/%v : %v/%v", ob1.Asks[0].Price, ob1.Bids[0].Price, ob2.Asks[0].Price, ob2.Bids[0].Price)
		return nil
	}
	fullBid := bids[i].Amount
	fullAsk := asks[j].Amount
	bidAmount := bids[i].Amount
	askAmount := asks[j].Amount
	for bids[i].Price > asks[j].Price {
		if bidAmount == askAmount {
			bidLeft = 0
			askLeft = 0
			mx := maxLeft / bids[i].Price
			if mx < bidAmount {
				bidLeft = bidAmount - mx
				askLeft = bidLeft
				log.Printf("Warning: ChekArbOpportunity - Arbitrage opportunity truncated due to max limit or not enough funds. Max: is set to %v", max)
				break
			}
			if i + 1 >= len(bids) {
				break
			}
			i++
			if j + 1 >= len(asks) {
				break
			}
			j++
			bidAmount = bids[i].Amount
			askAmount = asks[j].Amount
			fullBid = bidAmount
			fullAsk = askAmount
		} else if bidAmount > askAmount {
			mx := maxLeft / asks[j].Price
			if mx < askAmount {
				bidLeft = bidAmount - mx 
				askLeft = askAmount - mx
				log.Printf("Warning: ChekArbOpportunity - Arbitrage opportunity truncated due to max limit or not enough funds. Max: is set to %v", max)
				break
			}
			maxLeft = maxLeft - askAmount * asks[j].Price
			bidAmount = bidAmount - askAmount
			bidLeft = bidAmount
			askLeft = 0
			if j + 1 >= len(asks) {
				break
			}
			j++
			askAmount = asks[j].Amount
			fullAsk = askAmount
		} else { // bidAmount < askAmount
			mx := maxLeft / bids[i].Price
			if mx < bidAmount {
				bidLeft = bidAmount - mx
				askLeft = askAmount - mx 
				log.Printf("Warning: ChekArbOpportunity - Arbitrage opportunity truncated due to max limit or not enough funds. Max: is set to %v", max)
				break
			}
			maxLeft = maxLeft - bidAmount * bids[i].Price
			askAmount = askAmount - bidAmount
			askLeft = askAmount
			bidLeft = 0
			if i + 1 >= len(bids) {
				break
			}
			i++
			bidAmount = bids[i].Amount
			fullBid = bidAmount
		}
	}
	if bidLeft == 0 {
		i = i - 1
	}
	if askLeft == 0 {
		j = j - 1
	}
	log.Printf("There is an arbitrage opportunity ask/bid : ask/bid = %v/%v : %v/%v", ob1.Asks[0].Price, ob1.Bids[0].Price, ob2.Asks[0].Price, ob2.Bids[0].Price)
	return &ArbitrageOppCheckResult{BidNdx: i, AskNdx: j, BidLeft: (1-bidLeft/fullBid), AskLeft: (1-askLeft/fullAsk)}
}