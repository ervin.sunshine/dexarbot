package orderbook

import (
	"testing"
	"fmt"
	"gitlab.com/zlyzol/dexarbot/ctx"
)

func TestMerge(t *testing.T) {

	_, obBnbUsd, obRuneBnb, expectedObRuneUsdMerged := GetTestOrderbooks()
	obRuneUsdMerged := Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})

	// orederbook merge test 1
	for i, v := range obRuneUsdMerged.Bids {
		v2 := expectedObRuneUsdMerged.Bids[i]
		if ctx.F64xI64(v.Amount) != ctx.F64xI64(v2.Amount) {
			t.Errorf("Merge test 1 error in %v-th Bid. (%v) != (%v)", i, ctx.F64xI64(v.Amount), ctx.F64xI64(v2.Amount))
		}
	}
	for i, v := range obRuneUsdMerged.Asks {
		v2 := expectedObRuneUsdMerged.Asks[i]
		if ctx.F64xI64(v.Amount) != ctx.F64xI64(v2.Amount) {
			t.Errorf("Merge test 1 error in %v-th Ask. (%v) != (%v)", i, ctx.F64xI64(v.Amount), ctx.F64xI64(v2.Amount))
		}
	}
	fmt.Println("orderbook merge test 1 passed")
	// orederbook merge test 1 end

	// orederbook merge test 2
	obRuneBnb.Bids[0].Price = 0.00690242
	obRuneUsdMerged = Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})
	if ctx.F64xI64(obRuneUsdMerged.Bids[0].Price) != ctx.F64xI64(0.1193304174) {
		t.Errorf("Merge test 2 error in 1. Bid. (%v) != (%v)", ctx.F64xI64(obRuneUsdMerged.Bids[0].Price), ctx.F64xI64(0.119330244562))
	}
	fmt.Println("orderbook merge test 2 passed")
	// orederbook merge test 2 end

	// orederbook merge test 3
	obRuneBnb.Asks[0].Price = 0.00670005
	obRuneBnb.Asks[1].Price = 0.00679898
	obRuneUsdMerged = Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})
	if ctx.F64xI64(obRuneUsdMerged.Asks[0].Price) != ctx.F64xI64(0.116854232) || ctx.F64xI64(obRuneUsdMerged.Asks[1].Price) != ctx.F64xI64(0.1185796504) {
		t.Errorf("Merge test 3 error in 1. or 2. Ask. (%v) != (%v) or (%v) != (%v)", ctx.F64xI64(obRuneUsdMerged.Asks[0].Price), ctx.F64xI64(0.1168620804), ctx.F64xI64(obRuneUsdMerged.Asks[1].Price), ctx.F64xI64(0.1185796504))
	}
	fmt.Println("orderbook merge test 3 passed")
	// orederbook merge test 3 end
}

func TestChekArbOpportunity(t *testing.T) {
	obRuneUsd, obBnbUsd, obRuneBnb, _ := GetTestOrderbooks()
	obRuneUsdMerged := Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})
	// arb opp test 1
	arbRes := ChekArbOpportunity(obRuneUsd, obRuneUsdMerged, 10000)
	if arbRes != nil {
		t.Error("Found arbitrage opportunity where it shoudnt be found")
	}
	fmt.Println("Arbitrage opportunity phase 1 passed")
	// arb opp test 1 end

	// arb opp test 2
	obRuneBnb.Bids[0].Price = 0.00690242
	obRuneUsdMerged = Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})
	arbRes = ChekArbOpportunity(obRuneUsd, obRuneUsdMerged, 10000)
	if arbRes == nil {
		t.Errorf("Not found arbitrage opportunity where it shoud be found")
	}
	fmt.Println("Arbitrage opportunity phase 2 passed")
	if arbRes.BidNdx != 0 || arbRes.AskNdx != 2 || arbRes.AskLeft != 1 || ctx.F64xI64(arbRes.BidLeft) != ctx.F64xI64(0.3954423592) {
		t.Errorf("Arbitrage opp check phase 2 bad result. Expected (0, 2, 1, %v), Get (%v, %v, %v, %v)", ctx.F64xI64(0.3954423592), arbRes.BidNdx, arbRes.AskNdx, arbRes.AskLeft, ctx.F64xI64(arbRes.BidLeft))
	}
	fmt.Println("Arbitrage opp check phase 2 passed")
	// arb opp test 2 end

	// arb opp test 3
	obRuneBnb.Asks[0].Price = 0.00670005
	obRuneBnb.Asks[1].Price = 0.00679898
	obRuneUsdMerged = Merge(obRuneBnb, obBnbUsd, [2]uint64{100, 100000000})
	arbRes = ChekArbOpportunity(obRuneUsd, obRuneUsdMerged, 10000)
	if arbRes == nil {
		t.Errorf("Not found arbitrage opportunity where it shoud be found")
	}
	fmt.Println("Arbitrage opportunity phase 3 passed")
	if arbRes.BidNdx != 1 || arbRes.AskNdx != 1 || arbRes.BidLeft != 1 || ctx.F64xI64(arbRes.AskLeft) != ctx.F64xI64(0.05607972583) {
		t.Errorf("Arbitrage opp check phase 3 bad result. Expected (1, 1, 1, %v), Get (%v, %v, %v, %v)", ctx.F64xI64(0.05607972583), arbRes.BidNdx, arbRes.AskNdx, arbRes.BidLeft, ctx.F64xI64(arbRes.AskLeft))
	}
	fmt.Println("Arbitrage opp check phase 3 passed")
	// arb opp test 3 end

	// arb opp test 4 - test less max limit then arb opportunity
	arbRes = ChekArbOpportunity(obRuneUsd, obRuneUsdMerged, 30)
	if arbRes == nil {
		t.Errorf("Not found arbitrage opportunity where it shoud be found")
	}
	fmt.Println("Arbitrage opportunity phase 4 passed")
	if arbRes.BidNdx != 1 || arbRes.AskNdx != 1 || ctx.F64xI64(arbRes.BidLeft) != ctx.F64xI64(0.2656043688) || ctx.F64xI64(arbRes.AskLeft) != ctx.F64xI64(0.0311947073099) {
		t.Errorf("Arbitrage opp check phase 4 bad result. Expected (1, 1, %v, %v), Get (%v, %v, %v, %v)", ctx.F64xI64(0.2656043688), ctx.F64xI64(0.0311947073099), arbRes.BidNdx, arbRes.AskNdx, ctx.F64xI64(arbRes.BidLeft), ctx.F64xI64(arbRes.AskLeft))
	}
	fmt.Println("Arbitrage opp check phase 4 passed")
	// arb opp test 4 end
}
func GetTestOrderbooks() (*Orderbook, *Orderbook, *Orderbook, *Orderbook) {
	e0 := SimpleOrderbookEntry{Price: 0, Amount: 0}
	obRuneUsd := &Orderbook{
		Bids: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 0.119217, Amount: 200.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.118967, Amount: 200.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.116835, Amount: 2177.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.115371, Amount: 200.000000}, e0},
		},
		Asks: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 0.119301, Amount: 200.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.119302, Amount: 43.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.119318, Amount: 52.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.119720, Amount: 5020.000000}, e0},
		},
	}

	obBnbUsd := &Orderbook{
		Bids: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 17.288200, Amount: 5.264000}, e0},
			{SimpleOrderbookEntry{Price: 17.229400, Amount: 14.578000}, e0},
			{SimpleOrderbookEntry{Price: 17.170500, Amount: 5.960000}, e0},
			{SimpleOrderbookEntry{Price: 17.112200, Amount: 3.925000}, e0},
		},
		Asks: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 17.440800, Amount: 40.592000}, e0},
			{SimpleOrderbookEntry{Price: 17.463200, Amount: 17.523000}, e0},
			{SimpleOrderbookEntry{Price: 17.467800, Amount: 30.418000}, e0},
			{SimpleOrderbookEntry{Price: 17.490400, Amount: 13.090000}, e0},
		},
	}

	obRuneBnb := &Orderbook{
		Bids: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 0.00670003, Amount: 746.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00670000, Amount: 2087.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00665001, Amount: 2000.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00665000, Amount: 10000.000000}, e0},
		},
		Asks: OrderbookEntries{
			{SimpleOrderbookEntry{Price: 0.00699896, Amount: 69.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00699898, Amount: 13724.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00699900, Amount: 16854.000000}, e0},
			{SimpleOrderbookEntry{Price: 0.00704878, Amount: 77548.000000}, e0},
		},
	}

	expectedObRuneUsdMerged := &Orderbook{
		Bids: OrderbookEntries{
			{SimpleOrderbookEntry{0.115831458646, 745.9999999999999}, SimpleOrderbookEntry{17.2882, 4.99822238}},
			{SimpleOrderbookEntry{0.11583094000000001, 39.6683014925374}, SimpleOrderbookEntry{17.2882, 0.2657776200000006}},
			{SimpleOrderbookEntry{0.11543698, 2047.3316985074625}, SimpleOrderbookEntry{17.2294, 13.71712238}},
			{SimpleOrderbookEntry{0.11457568229399998, 129.45508653370447}, SimpleOrderbookEntry{17.2294, 0.8608776200000001}},
			{SimpleOrderbookEntry{0.114183996705, 896.2392537755583}, SimpleOrderbookEntry{17.1705, 5.96}},
			{SimpleOrderbookEntry{0.113796301122, 590.2246763538701}, SimpleOrderbookEntry{17.1122, 3.925}},
		},
		Asks: OrderbookEntries{
			{SimpleOrderbookEntry{0.122067461568, 69}, SimpleOrderbookEntry{17.4408, 0.48292824}},
			{SimpleOrderbookEntry{0.122067810384, 5730.702439498326}, SimpleOrderbookEntry{17.4408, 40.10907176}},
			{SimpleOrderbookEntry{0.122224587536, 2503.6505319346534}, SimpleOrderbookEntry{17.4632, 17.523}},
			{SimpleOrderbookEntry{0.122256782844, 4346.0618547274025}, SimpleOrderbookEntry{17.4678, 30.418}},
			{SimpleOrderbookEntry{0.12241495979200001, 1143.5851738396177}, SimpleOrderbookEntry{17.4904, 8.003929760000007}},
			{SimpleOrderbookEntry{0.12241530960000001, 726.6852750392902}, SimpleOrderbookEntry{17.4904, 5.086070239999993}},
		},
	}	
	return obRuneUsd, obBnbUsd, obRuneBnb, expectedObRuneUsdMerged
}
