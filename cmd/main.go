package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/zlyzol/dexarbot/api/router"
	"gitlab.com/zlyzol/dexarbot/api/server"
	"gitlab.com/zlyzol/dexarbot/bot"
	"gitlab.com/zlyzol/dexarbot/ctx"
)

func main() {
	ctx := ctx.New("")
	defer ctx.Close()
	bot := bot.New(ctx)
	bot.Start()
	defer bot.Stop()
	srv := server.New(ctx, bot)
	appRouter := router.New(srv)
	address := ctx.Config.ListenPort
	log.Printf("Starting server %s\n", address)
	s := &http.Server{
		Addr:         address,
		Handler:      appRouter,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  120 * time.Second,
	}
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Server startup failed: %v", err)
	}
}
