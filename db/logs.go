package db

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/zlyzol/dexarbot/ctx"
)

type Logs []*Log
type Log struct {
	Text string `json:"text"`
}

func GetLogs(ctx *ctx.Context, limit uint64) (Logs, error) {
	logs := make([]*Log, 0)
	f, err := os.OpenFile(ctx.Config.LogFile, os.O_RDONLY, 0)
	if err != nil {
		err := fmt.Errorf("Error: Log file open for reading logs failed: %v", err)
		log.Print(err.Error())
		return logs, err
	}
	defer f.Close()

	stat, err := os.Stat(ctx.Config.LogFile)
	maxLim := int64(1024 * limit) // assume 1 line is less then 1024 bytes
	if stat.Size() < maxLim {
		maxLim = stat.Size()
	}
	start := stat.Size() - maxLim
	buf := make([]byte, maxLim)
	_, err = f.ReadAt(buf, start)
	sliceData := strings.Split(string(buf), "\n")
	lines := len(sliceData)
	if lines == 0 {
		return logs, nil
	}
	if len(sliceData[lines-1]) == 0 {
		lines--
	}
	logs = make([]*Log, min(int64(lines), int64(limit)))
	var j uint64 = 0
	for i := lines - 1; i >= 0 && j < limit; i-- {
		logs[j] = &Log{Text: sliceData[i]}
		j++
	}
	return logs, nil
}

func min(a int64, b int64) int64 {
	if a < b {
		return a
	}
	return b
}
