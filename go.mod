require (
	github.com/binance-chain/go-sdk v1.2.2
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/golang/protobuf v1.4.0 // indirect
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200420144010-e5e8543f8aeb // indirect
	google.golang.org/grpc v1.29.0
)

replace github.com/tendermint/go-amino => github.com/binance-chain/bnc-go-amino v0.14.1-binance.1

module gitlab.com/zlyzol/busdbot

go 1.13
